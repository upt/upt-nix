# Universal Packaging Tool: Nix backend
This is a [Nix](https://nixos.org/) backend for
[upt](https://pypi.python.org/pypi/upt).
